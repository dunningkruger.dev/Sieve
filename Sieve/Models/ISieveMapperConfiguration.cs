﻿using Sieve.Services;

namespace Sieve.Models
{
    public interface ISieveMapperConfiguration
    {
        void MapProperties(SievePropertyMapper mapper);
    }
}
